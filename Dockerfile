FROM ubuntu:16.04
RUN apt-get update && apt-get -y install git apache2
RUN cd /var/www && git clone http://bitbucket.org/jfbethlehem/dxctf_challenge1 html
RUN rm -rf /var/www/html/.git
RUN /etc/init.d/apache2 restart
